﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Diagnostics;
using System.Web.UI.WebControls;
using HtmlAgilityPack;
using ScrapySharp;
using ScrapySharp.Html;
using ScrapySharp.Html.Forms;
using ScrapySharp.Extensions;
using ScrapySharp.Network;
using BankAccountManager.Model;
using BankAccountManager.Strategy.UserManagers;
using BankAccountManager.Facade;
using BankAccountManager.Enum;
using BankAccountManager.Strategy.TableParsers;
using BankAccountManager.Strategy.TableProviders;
using System.Net;
using System.Data.Entity.Migrations;
using EntityFramework.Extensions;
using BankAccountManager.Pages;

namespace BankAccountManager
{
    public partial class _Default : BankAccountManagerPage
    {
        public BankUser currentUser { get; set; }
        public ScrapingBrowser browser { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //instance browser object
            browser = new ScrapingBrowser();
        }

        public void BuildBankUser()
        {
            //Get user inputs
            string userName = LoginForm.UserName;
            string userPass = LoginForm.Password;
            currentUser = new BankUser() { Name = userName, Pass = userPass };
            //save current user
            Session["currentUser"] = userName;
        }

        public void initObjectsByBankType(BankType bankType)
        {
            switch (bankType)
            {
                case BankType.BANCO_POPULAR:
                    managerFacade = new BankAccountManagerFacade(new BPDHtmlTableProviderStrategy(), new BPDHtmlTableParserStrategy(), browser);
                    userManagerStrategy = new BPDUserManagerStrategy();
                    userManagerStrategy.Browser = browser;
                    break;
            }

            //save manager facade in session
            Session["managerFacade"] = managerFacade;
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            //Set default strategy to the BPD (Banco Popular Dominicano)
            initObjectsByBankType(BankType.BANCO_POPULAR);

            BuildBankUser();
            userManagerStrategy.LogUser(currentUser);
            UserIsLogged = userManagerStrategy.UserIsLogged();
            Debug.WriteLine("User is logged?: " + UserIsLogged);

            if (UserIsLogged)
            {
                Response.Redirect("Accounts.aspx");
            }
            else
            {
                //Debug.WriteLine("Failed to log the user.");
                Response.Redirect("DefaultError.aspx");
            }
        }
    }
}