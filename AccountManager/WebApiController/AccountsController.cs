﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BankAccountManager.Model;
using BankAccountManager.Filters;

namespace BankAccountManager.WebApiController
{
    [BasicAuthenticationFilter]
    public class AccountsController : ApiController
    {
        public IEnumerable<UserAccountModel> GetAllAccounts()
        {
            using (var dbContext = new BankAccountManagerContext())
            {
                List<UserAccountModel> accountsList = new List<UserAccountModel>();
               
                foreach (var account in dbContext.UserAccounts)
                {
                    UserAccountModel accountModel = new UserAccountModel();
                    accountModel.UserAccountId = account.UserAccountId;
                    accountModel.User = account.User;
                    accountModel.Alias = account.Alias;
                    accountModel.Number = account.Number;
                    accountModel.Type = account.Type;
                    accountModel.Coin = account.Coin;
                    accountModel.Balance = account.Balance;
                    accountModel.UserTransactions = account.UserTransactions;
                    accountsList.Add(accountModel);
                }
                IEnumerable<UserAccountModel> accounts = accountsList;

                return accounts;
            }
        }
    }
}