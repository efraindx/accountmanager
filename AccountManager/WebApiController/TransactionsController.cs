﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BankAccountManager.Model;

namespace AccountManager.WebApiController
{
    public class TransactionsController : ApiController
    {
        private List<UserTransaction> transactions;
        public TransactionsController()
        {
            using (var dbContext = new BankAccountManagerContext())
            {
                transactions = dbContext.UserTransactions.ToList();
            }
        }

        public IEnumerable<UserTransactionModel> GetAllTransactions()
        {
            List<UserTransactionModel> transactionsList = new List<UserTransactionModel>();
          
            foreach (var transaction in transactions)
            {
                UserTransactionModel transactionModel = new UserTransactionModel();
                transactionModel.UserTransactionId = transaction.UserTransactionId;
                transactionModel.PosteoDate = transaction.PosteoDate;
                transactionModel.EffectiveDate = transaction.EffectiveDate;
                transactionModel.CheckNumber = transaction.CheckNumber;
                transactionModel.Description = transaction.Description;
                transactionModel.Amount = transaction.Amount;
                transactionModel.Balance = transaction.Balance;
                transactionModel.UserAccountId = transaction.UserAccountId;
                transactionsList.Add(transactionModel);
            }
            IEnumerable<UserTransactionModel> userTransactionsList = transactionsList;

            return userTransactionsList;
        }

        public UserTransactionModel GetTransactionByAccountID(int accountID)
        {
            UserTransaction transaction = transactions.Where(t => t.UserAccountId == accountID).FirstOrDefault();
            if (null != transaction)
            {
                UserTransactionModel userTransactionModel = new UserTransactionModel();
                userTransactionModel.UserTransactionId = transaction.UserTransactionId;
                userTransactionModel.PosteoDate = transaction.PosteoDate;
                userTransactionModel.EffectiveDate = transaction.EffectiveDate;
                userTransactionModel.CheckNumber = transaction.CheckNumber;
                userTransactionModel.Description = transaction.Description;
                userTransactionModel.Amount = transaction.Amount;
                userTransactionModel.Balance = transaction.Balance;
                userTransactionModel.UserAccountId = transaction.UserAccountId;

                return userTransactionModel;
            }

            return null;
        }
    }
}