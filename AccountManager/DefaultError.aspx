﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BankAccountManager._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <%--  <asp:Login ID="Login1" runat="server" OnAuthenticate="ValidateUser" CssClass="row">
        <div class="row" runat="server" id="formLog>
            <div class="col-xs-offset-2 col-xs-9">
                <table>
                    <tr>
                        <th>Usuario:</th>
                        <td>
                            <asp:TextBox CssClass="form-control" placeholder="Tu Usuario" ID="UserNameTextBox" runat="server" />
                        </td>
                        <th>Contraseña:</th>
                        <td>
                            <asp:TextBox CssClass="form-control" TextMode="Password" placeholder="Tu Contraseña" ID="UserPassTextBox" runat="server" />
                        </td>
                        <td>
                            <asp:Button ID="SeeAccountsButton" CssClass="btn btn-success" runat="server" Text="Ver Cuentas" OnClick="SeeAccountsButton_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-xs-3" />
        </div>
    </asp:Login> --%>

    <asp:Login ID="LoginForm" runat="server" CssClass="form-signin">
        <LayoutTemplate>
            <p class="validation-summary-errors">
                <label class="field-validation-error">Usuario y/o Contraseña incorrecto</label>
            </p>
            <h2 class="form-signin-heading">Inicio de Sesión</h2>
            <asp:TextBox CssClass="form-control" placeholder="Tu Usuario" ID="UserName" runat="server" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName" CssClass="field-validation-error" ErrorMessage="El usuario es requerido." />
            <asp:TextBox CssClass="form-control" TextMode="Password" placeholder="Tu Contraseña" ID="Password" runat="server" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="field-validation-error" ErrorMessage="La contraseña es requerida." />

            <asp:Button ID="LoginButton" CssClass="btn btn-lg btn-primary btn-block" runat="server" Text="Iniciar Sesión" OnClick="LoginButton_Click" />
        </LayoutTemplate>
    </asp:Login>
</asp:Content>
