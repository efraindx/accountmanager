﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BankAccountManager.Facade;
using BankAccountManager.Model;
using EntityFramework.Extensions;
using System.Diagnostics;
using BankAccountManager.Pages;

namespace BankAccountManager
{
    public partial class Transactions : BankAccountManagerPage
    {
        public List<UserTransaction> transactions { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        private List<UserTransaction> getTransactions()
        {
            BankAccountManagerFacade managerFacade = (BankAccountManagerFacade)Session["managerFacade"];
            UserAccount selectedAccount = (UserAccount)Session["selectedAccount"];
            Debug.WriteLine("selectedAccount alias other: " + selectedAccount.Alias);


            if (null != managerFacade)
            {
                transactions = managerFacade.GetUserTransactionsListByUserAccount(selectedAccount);
                SaveTransactionsToDB(transactions);
                return transactions;
            }
            return null;
        }

        //Add or Replace transactions in DB
        private void SaveTransactionsToDB(List<UserTransaction> transactions)
        {
            UserAccount selectedAccount = (UserAccount)(Session["selectedAccount"]);

            using (var dbContext = new BankAccountManagerContext())
            {
                bool accountHasTransactions = dbContext.UserTransactions.FirstOrDefault(t => t.UserAccountId == selectedAccount.UserAccountId) != null;

                if (accountHasTransactions)
                {
                    Debug.WriteLine("account has transactions");
                    //delete transactions of account
                    dbContext.UserTransactions.Where(t => t.UserAccountId == selectedAccount.UserAccountId).Delete();
                    dbContext.SaveChanges();

                    //add new transactions
                    dbContext.UserTransactions.AddRange(transactions);
                    dbContext.SaveChanges();
                }
                else
                {
                    Debug.WriteLine("account has not transactions");
                    dbContext.UserTransactions.AddRange(transactions);
                    dbContext.SaveChanges();
                }
            }
        }

        protected void DataPagerTransactions_PreRender(object sender, EventArgs e)
        {
            List<UserTransaction> transactions = getTransactions();
            this.transactionsList.DataSource = transactions;
            //update list view
            this.transactionsList.DataBind();
        }
    }
}