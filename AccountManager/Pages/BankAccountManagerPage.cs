﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using BankAccountManager.Strategy.UserManagers;
using BankAccountManager.Facade;
using BankAccountManager.Model;
using System.Diagnostics;
using EntityFramework.Extensions;

namespace BankAccountManager.Pages
{
    public abstract partial class BankAccountManagerPage : Page
    {
        public UserManagerStrategy userManagerStrategy { get; set; }
        public BankAccountManagerFacade managerFacade { get; set; }
        public bool UserIsLogged { get; set; }
    }
}