﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Accounts.aspx.cs" Inherits="BankAccountManager.Accounts" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <asp:Label Text="Cuentas" ID="TableHeaderLabel" runat="server" />
                    <br />
                    Seleccione una cuenta para ver sus transacciones</div>
            </div>

            <asp:ListView ID="accountsList" runat="server" OnSelectedIndexChanged="accountsList_SelectedIndexChanged"
                ItemType="BankAccountManager.Model.UserAccount" SelectMethod="accountsList_GetData" DataKeyNames="Alias">
                <EmptyDataTemplate>
                    <tr>
                        <td>No hay datos para mostrar.</td>
                    </tr>
                </EmptyDataTemplate>
                <LayoutTemplate>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Alias</th>
                                <th>Número</th>
                                <th>Tipo</th>
                                <th>Moneda</th>
                                <th>Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="itemPlaceholder" runat="server"></tr>
                        </tbody>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:LinkButton ID="AccountLinkButton" runat="server" Text="<%#Item.Alias %>" 
                               CommandName="Select" /></td>
                        <td><%#Item.Number %></td>
                        <td><%#Item.Type %></td>
                        <td><%#Item.Coin %></td>
                        <td><%# string.Format("{0:0,0.00}", Item.Balance) %></td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
            <div class="col-xs-1" />
        </div>
    </div>
    </div>
    </asp:Content>