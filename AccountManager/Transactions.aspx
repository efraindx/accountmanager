﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Transactions.aspx.cs" Inherits="BankAccountManager.Transactions" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
     <div class="row">
        <div class="col-xs-10 col-xs-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <asp:Label Text="Transacciones" ID="TableHeaderLabel" runat="server" />
                </div>
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-success" NavigateUrl="~/Accounts.aspx">Volver a Cuentas</asp:HyperLink>
            </div>

            <asp:ListView ID="transactionsList" runat="server"
              ItemType="BankAccountManager.Model.UserTransaction">
                <EmptyDataTemplate>
                    <tr>
                        <td>No hay datos para mostrar.</td>
                    </tr>
                </EmptyDataTemplate>
                <LayoutTemplate>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Fecha Posteo</th>
                                <th>Descripción</th>
                                <th>Monto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="itemPlaceholder" runat="server"></tr>
                        </tbody>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%#Item.PosteoDate %></td>
                        <td><%#Item.Description %></td>
                        <td><%# string.Format("{0:0,0.00}", Item.Amount) %></td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>

            <asp:DataPager ID="DataPagerTransactions" runat="server" PagedControlID="transactionsList" PageSize="10"
                    OnPreRender="DataPagerTransactions_PreRender">
                <Fields>
                    <asp:NextPreviousPagerField ShowFirstPageButton="true" ShowNextPageButton="false"/>
                    <asp:NumericPagerField />
                    <asp:NextPreviousPagerField ShowLastPageButton="true" ShowPreviousPageButton="false"/>
                </Fields>
            </asp:DataPager>

            <div class="col-xs-1" />
        </div>
    </div>
</div>
</asp:Content>