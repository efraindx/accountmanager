﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BankAccountManager.Model
{
    public class BankAccountManagerContext : DbContext
    {
        public DbSet<UserAccount> UserAccounts { get; set; }
        public DbSet<UserTransaction> UserTransactions { get; set; }
    }

	public class UserAccount
	{
        public int UserAccountId { get; set;}
        public string User { get; set; }
        public string Alias { get; set;}
        public long Number { get; set; }
        public string Type { get; set; }
        public string Coin { get; set; }
        public double Balance { get; set; }
        public virtual List<UserTransaction> UserTransactions { get; set; }
	}
}