﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankAccountManager.Model
{
    public class BankUser
    {
        public string Name { get; set; }
        public string Pass { get; set; }
    }
}