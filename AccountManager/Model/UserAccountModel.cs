﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankAccountManager.Model
{
    public class UserAccountModel
    {
        public int UserAccountId { get; set; }
        public string User { get; set; }
        public string Alias { get; set; }
        public long Number { get; set; }
        public string Type { get; set; }
        public string Coin { get; set; }
        public double Balance { get; set; }
        public List<UserTransaction> UserTransactions { get; set; }
    }
}