﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BankAccountManager.Model;
using System.Web.UI.WebControls;
using System.Diagnostics;
using BankAccountManager.Facade;
using BankAccountManager.Pages;
using EntityFramework.Extensions;

namespace BankAccountManager
{
    public partial class Accounts : BankAccountManagerPage
    {
        public List<UserAccount> userAccountList { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            managerFacade = (BankAccountManagerFacade)(Session["managerFacade"]);

            if (null != managerFacade)
            {
                userAccountList = managerFacade.GetUserAccountsList();
                SaveUserAccountsToDB(userAccountList);
                //update listview with new values
                //accountsList.DataBind();
            }
        }

        //Add or Replace accounts in DB
        private void SaveUserAccountsToDB(List<UserAccount> userAccounts)
        {
            string currentUser = (string)(Session["currentUser"]);

            using (var dbContext = new BankAccountManagerContext())
            {
                bool userHasAccounts = dbContext.UserAccounts.FirstOrDefault(a => a.User == currentUser) != null;

                if (userHasAccounts)
                {
                    Debug.WriteLine("user has accounts");
                    //delete old accounts
                    dbContext.UserAccounts.Where(a => a.User == currentUser).Delete();
                    dbContext.SaveChanges();


                    dbContext.UserAccounts.AddRange(userAccounts);
                    dbContext.SaveChanges();
                }
                else
                {
                    Debug.WriteLine("user no has accounts");
                    dbContext.UserAccounts.AddRange(userAccounts);
                    dbContext.SaveChanges();
                }
            }
        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        public IQueryable<UserAccount> accountsList_GetData()
        {
            userAccountList = GetUserAccountsFromManagerStrategy();

            if (null != userAccountList)
            {
                //Debug.WriteLine("accounts not null, size: " + accounts.Count);
                return userAccountList.AsQueryable();
            }
            //Debug.WriteLine("accounts null");
            return null;
        }

        private List<UserAccount> GetUserAccountsFromManagerStrategy()
        {
            managerFacade = (BankAccountManagerFacade)(Session["managerFacade"]);

            if (null != managerFacade)
            {
                //Debug.WriteLine("accounts not null, size: " + accounts.Count);
                return managerFacade.GetUserAccountsList();
            }

            return null;
        }

        protected void accountsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string currentUser = (string)(Session["currentUser"]);
            UserAccount selectedAccount = FindUserAccountInDBByAliasAndUser(accountsList.SelectedValue.ToString(), currentUser);
            Session["selectedAccount"] = selectedAccount;
            Debug.WriteLine("selected Account alias: " + selectedAccount.Alias);
            Response.Redirect("Transactions.aspx");
            base.OnLoad(e);
        }

        private UserAccount FindUserAccountInDBByAliasAndUser(string alias, string currentUser)
        {
            using (var dbContext = new BankAccountManagerContext())
            {
                return dbContext.UserAccounts.Where(a => alias.Equals(a.Alias) && a.User == currentUser).SingleOrDefault();
            }
        }
    }
}