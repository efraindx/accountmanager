﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BankAccountManager.Strategy.TableProviders;
using BankAccountManager.Strategy.TableParsers;
using BankAccountManager.Model;
using HtmlAgilityPack;
using ScrapySharp.Network;

namespace BankAccountManager.Facade
{
    public class BankAccountManagerFacade
    {
        public HtmlTableProviderStrategy _htmlTableProviderStrategy { get; set; }
        public HtmlTableParserStrategy _htmlTableParserStrategy { get; set; }

        public BankAccountManagerFacade(HtmlTableProviderStrategy _htmlTableProviderStrategy, HtmlTableParserStrategy _htmlTableParserStrategy, ScrapingBrowser browser)
        {
            this._htmlTableProviderStrategy = _htmlTableProviderStrategy;
            this._htmlTableProviderStrategy.Browser = browser;
            this._htmlTableParserStrategy = _htmlTableParserStrategy;
            this._htmlTableParserStrategy.Browser = browser;
        }

        public List<UserAccount> GetUserAccountsList()
        {
            HtmlNode userAccountsHtmlTable = _htmlTableProviderStrategy.GetUserAccountsHTMLTable();
            return _htmlTableParserStrategy.ParseUserAccountsHtmlTableAsList(userAccountsHtmlTable);
        }

        public List<UserTransaction> GetUserTransactionsListByUserAccount(UserAccount userAccount)
        {
            HtmlNode userTransactionsHtmlTable = _htmlTableProviderStrategy.GetUserTransactionsHtmlTableByAccount(userAccount);
            return _htmlTableParserStrategy.ParseUserTransactionsHtmlTableAsList(userTransactionsHtmlTable);
        }
    }
}