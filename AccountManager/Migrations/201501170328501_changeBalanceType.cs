namespace AccountManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeBalanceType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserAccounts", "Balance", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserAccounts", "Balance", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
