namespace AccountManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserTransactionClass : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserTransactions",
                c => new
                    {
                        UserTransactionId = c.Int(nullable: false, identity: true),
                        PosteoDate = c.String(),
                        EffectiveDate = c.String(),
                        CheckNumber = c.Long(nullable: false),
                        Description = c.String(),
                        Amount = c.String(),
                        Balance = c.String(),
                        UserAccountId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserTransactionId)
                .ForeignKey("dbo.UserAccounts", t => t.UserAccountId, cascadeDelete: true)
                .Index(t => t.UserAccountId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserTransactions", "UserAccountId", "dbo.UserAccounts");
            DropIndex("dbo.UserTransactions", new[] { "UserAccountId" });
            DropTable("dbo.UserTransactions");
        }
    }
}
