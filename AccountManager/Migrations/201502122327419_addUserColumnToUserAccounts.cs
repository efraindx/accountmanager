namespace AccountManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUserColumnToUserAccounts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserAccounts", "User", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserAccounts", "User");
        }
    }
}
