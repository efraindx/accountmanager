namespace AccountManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCheckNumberType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserTransactions", "CheckNumber", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserTransactions", "CheckNumber", c => c.Long(nullable: false));
        }
    }
}
