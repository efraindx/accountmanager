﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BankAccountManager.Model;

namespace BankAccountManager.Strategy.UserManagers
{
    public abstract class UserManagerStrategy : BankAccountStrategy
    {
        //Algorithm to log the user
        public abstract void LogUser(BankUser bankUser);
        //Algorithm to know if the user is logged
        public abstract bool UserIsLogged();
    }
}