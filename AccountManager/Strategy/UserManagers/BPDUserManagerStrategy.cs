﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ScrapySharp.Network;
using ScrapySharp.Html.Forms;
using ScrapySharp.Html;
using ScrapySharp.Extensions;
using HtmlAgilityPack;
using BankAccountManager.Model;

namespace BankAccountManager.Strategy.UserManagers
{
    public class BPDUserManagerStrategy : UserManagerStrategy
    {
        public override void LogUser(BankUser bankUser)
        {
            // Navigate to BPD home page
            WebPage homePage = browser.NavigateToPage(new Uri("https://www.bpd.com.do/banco.popular.aspx"));
            WebPage loginPage = homePage.FindLinks(By.Text("&nbsp;&nbsp;Acceso Personal&nbsp;&nbsp;")).Single().Click();
            PageWebForm formLogin = loginPage.FindForm("frmLogin");

            formLogin["fldNombre"] = bankUser.Name;
            formLogin["fldPassword"] = bankUser.Pass;
            formLogin.Method = HttpVerb.Post;

            //send form with values
            formLogin.Submit();
        }

        public override bool UserIsLogged()
        {
            WebPage AccountsPage = Browser.NavigateToPage(new Uri("https://www.bpd.com.do/banco.popular.aspx"));
            // Debug.WriteLine("AccountsPage HTML: " + AccountsPage.Html.InnerHtml);
            HtmlNode AccountsLink = AccountsPage.Html.CssSelect("div.md-navi-linkitem a").Where(a => a.InnerText == "Cuentas").SingleOrDefault();
            if (null != AccountsLink)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}