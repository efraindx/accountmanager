﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BankAccountManager.Model;
using HtmlAgilityPack;
using ScrapySharp.Extensions;
using System.Diagnostics;
using ScrapySharp.Network;
using System.IO;
using System.Net;
using CsvHelper;
using CsvHelper.Configuration;

namespace BankAccountManager.Strategy.TableParsers
{
    public class BPDHtmlTableParserStrategy : HtmlTableParserStrategy
    {
        public override List<UserAccount> ParseUserAccountsHtmlTableAsList(HtmlNode userAccountsHTMLTable)
        {
            //get current user from session
            HttpContext context = HttpContext.Current;
            string currentUser = (string)(context.Session["currentUser"]);

            var accounts = from table in userAccountsHTMLTable.ChildNodes
                           from row in table.CssSelect("tr.md-maketable-reg-tr").Cast<HtmlNode>()
                           // let cells = row.CssSelect("td.md-maketable-reg-td").ToArray()
                           let cells = row.CssSelect("td.lista1").ToArray().Length > 4 ? row.CssSelect("td.lista1").ToArray() : row.CssSelect("td.lista3").ToArray()
                           select new UserAccount
                           {
                               User = currentUser,
                               Alias = cells[0].InnerText.Replace("&nbsp;", ""),
                               Number = long.Parse(cells[1].InnerText.Replace("&nbsp;", "")),
                               Type = cells[3].InnerText.Replace("&nbsp;", ""),
                               Coin = cells[4].InnerText.Replace("&nbsp;", ""),
                               Balance = double.Parse(cells[5].InnerText.Replace("&nbsp;", ""))
                           };
            return accounts.ToList<UserAccount>();
        }

        public override List<UserTransaction> ParseUserTransactionsHtmlTableAsList(HtmlNode userTransactionsHTMLTable)
        {
            //get current user from session
            HttpContext context = HttpContext.Current;
            UserAccount selectedAccount = (UserAccount)(context.Session["selectedAccount"]);

            //Navigate to transactions page (the current url)
            WebPage transactionsBPDPage = browser.NavigateToPage(browser.Referer);
            Debug.WriteLine("transactions page html: " + transactionsBPDPage.Html.InnerHtml);

            //find link to export text csv file
            HtmlNode textExporterLink = transactionsBPDPage.Html.CssSelect("span.titulo2 a").ElementAt(0);
            //Debug.WriteLine("textExporterlinkhtml: " + textExporterLink.OuterHtml);
            //build the link address
            string link = "https://www.bpd.com.do/" + textExporterLink.GetAttributeValue("href", string.Empty);
            // Debug.WriteLine("link: " + link);

            //get temp file
            string tempFileName = Path.GetTempFileName();
            //download content of file
            var csvContent = browser.DownloadString(new Uri(link));
            //Debug.WriteLine("csvContent: " + csvContent);

            //write content to temp file
            File.WriteAllText(tempFileName, csvContent);

            //Debug.WriteLine("tempFIleName: " + tempFileName);
            //parse csv file
            var transactions = from line in File.ReadAllLines(tempFileName).Skip(1)
                               let columns = line.Split(',')
                               select new UserTransaction
                               {
                                   PosteoDate = columns[1],
                                   Amount = "$" + columns[3].TrimStart('0'),
                                   Description = columns[5],
                                   UserAccountId = selectedAccount.UserAccountId
                               };
            return transactions.ToList<UserTransaction>();
        }
    }
}