﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BankAccountManager.Model;
using HtmlAgilityPack;

namespace BankAccountManager.Strategy.TableParsers
{
    public abstract class HtmlTableParserStrategy : BankAccountStrategy
    {
        //Algorithm to parse account html table as objects
        public abstract List<UserAccount> ParseUserAccountsHtmlTableAsList(HtmlNode userAccountsHTMLTable);
        //Algorithm to parse transactions html table as objects
        public abstract List<UserTransaction> ParseUserTransactionsHtmlTableAsList(HtmlNode userTransactionsHTMLTable);
    }
}