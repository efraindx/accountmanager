﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using ScrapySharp.Html;
using ScrapySharp.Html.Forms;
using ScrapySharp.Network;
using ScrapySharp.Extensions;
using HtmlAgilityPack;
using BankAccountManager.Model;

namespace BankAccountManager.Strategy
{
    /*
     * Superclass for all AccountManager.Strategies
     * 
     * Implementing Strategy Design Pattern.
     */
    public abstract class BankAccountStrategy
    {
        protected ScrapingBrowser browser;

        public ScrapingBrowser Browser
        {
            get
            {
                return browser;
            }

            set
            {
                browser = value;
            }
        }
    }
}