﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;
using BankAccountManager.Model;

namespace BankAccountManager.Strategy.TableProviders
{
    public abstract class HtmlTableProviderStrategy : BankAccountStrategy
    {
        //Algorithm to get accounts html table 
        public abstract HtmlNode GetUserAccountsHTMLTable();
        //Algorithm to get user account transactions html table
        public abstract HtmlNode GetUserTransactionsHtmlTableByAccount(UserAccount UserAccount);
    }
}