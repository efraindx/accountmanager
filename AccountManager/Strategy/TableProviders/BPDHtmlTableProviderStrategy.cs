﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BankAccountManager.Model;
using HtmlAgilityPack;
using ScrapySharp.Network;
using ScrapySharp.Html;
using ScrapySharp.Html.Forms;
using ScrapySharp.Extensions;
using System.Diagnostics;

namespace BankAccountManager.Strategy.TableProviders
{
    public class BPDHtmlTableProviderStrategy : HtmlTableProviderStrategy
    {
        public override HtmlNode GetUserAccountsHTMLTable()
        {
            // Navigate to BPD home page
            WebPage homePage = Browser.NavigateToPage(new Uri("https://www.bpd.com.do/banco.popular.aspx"));
            Debug.WriteLine("homePage html: " + homePage.Html.InnerHtml);

            //For read local html (para no estar saliendo y entrando a la página, guardé como un html la página
            //donde se visualizan las cuentas y la guardé localmente) - Datos de prueba
            /* HtmlDocument doc = new HtmlDocument();
              string FileName = "Z:/Users/casa080/Desktop/Banco Popular - Internet Banking.html";
              doc.Load(FileName);
              //Get only the table that lists the accounts
              HtmlNode accountsTable = doc.DocumentNode.CssSelect("table.md-maketable").OfType<HtmlNode>().ToArray().ElementAt(0);
              */

            //Get only the table that lists the accounts
            HtmlNode accountsTable = homePage.Html.CssSelect("table.md-maketable").OfType<HtmlNode>().ToArray().ElementAt(0);

            return accountsTable;
        }

        public override HtmlNode GetUserTransactionsHtmlTableByAccount(UserAccount userAccount)
        {
            WebPage HomePage = Browser.NavigateToPage(new Uri("https://www.bpd.com.do/banco.popular.aspx"));

            HtmlNode accountLink = HomePage.Html.CssSelect("td.lista1 a").Where(a => a.InnerText == userAccount.Alias).Single();
            string transactionsAccountURL = "https://www.bpd.com.do/" + accountLink.GetAttributeValue("href", string.Empty);
            WebPage transactionsPage = Browser.NavigateToPage(new Uri(transactionsAccountURL), HttpVerb.Get, string.Empty);
            //Debug.WriteLine("Transations PAge HTML: " + transactionsPage.Html.InnerHtml);
            PageWebForm datesForm = transactionsPage.FindForm("DatesForm");

            //SerializeFormFields for get form attributes encoded
            string transaciontsPageUrl = "https://www.bpd.com.do/banco.popular.aspx?" + datesForm.SerializeFormFields();

            WebPage listTransactionsPage = Browser.NavigateToPage(new Uri(transaciontsPageUrl), HttpVerb.Get, string.Empty);

            //Debug.WriteLine("browser url: " + listTransactionsPage.AbsoluteUrl.AbsoluteUri);
            Debug.WriteLine("list transactions page: " + listTransactionsPage.Html.InnerHtml);

            //Get only the table that lists the accounts
            HtmlNode transactionsTable = listTransactionsPage.Html.CssSelect("table.md-maketable").OfType<HtmlNode>().ToArray().ElementAt(0);

            return transactionsTable;
        }
    }
}