﻿using System;
using System.Drawing;
using System.IO;
using Foundation;
using UIKit;

namespace BankAccountManager_MobileClient
{
	public partial class BankAccountManager_MobileClientViewController : UIViewController
	{
		public BankAccountManager_MobileClientViewController (IntPtr handle) : base (handle)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		#region View lifecycle

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			PasswordText.SecureTextEntry = true;
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			if (UserText.IsFirstResponder) {
				UserText.ResignFirstResponder();
			}

			if (PasswordText.IsFirstResponder) {
				PasswordText.ResignFirstResponder ();
			}
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
		}

		#endregion
	}
}

