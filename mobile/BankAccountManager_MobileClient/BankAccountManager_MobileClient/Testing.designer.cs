// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace BankAccountManager_MobileClient
{
	[Register ("Testing")]
	partial class Testing
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel txttest { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel txtText2 { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (txttest != null) {
				txttest.Dispose ();
				txttest = null;
			}
			if (txtText2 != null) {
				txtText2.Dispose ();
				txtText2 = null;
			}
		}
	}
}
