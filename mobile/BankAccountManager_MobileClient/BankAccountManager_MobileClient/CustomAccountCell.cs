﻿using System;
using System.Drawing;
using BankAccountManager_MobileClient.Model;

using Foundation;
using UIKit;

namespace BankAccountManager_MobileClient
{
	public partial class CustomAccountCell : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("CustomAccountCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("CustomAccountCell");
		public UserAccount Model { get; set; }

		public CustomAccountCell (IntPtr handle) : base (handle)
		{
		}

		public static CustomAccountCell Create ()
		{
			return (CustomAccountCell)Nib.Instantiate (null, null) [0];
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			this.AccountNumber.Text = Model.Number;
			this.AccountAlias.Text = Model.Alias;
			this.AccountBalance.Text = Model.Coin + " " + Model.Balance;
			this.AccountType.Text = Model.Type;
		}
	}
}

