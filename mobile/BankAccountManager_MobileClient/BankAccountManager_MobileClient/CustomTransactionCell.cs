﻿using System;
using System.Drawing;
using BankAccountManager_MobileClient.Model;
using Foundation;
using UIKit;

namespace BankAccountManager_MobileClient
{
	public partial class CustomTransactionCell : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("CustomTransactionCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("CustomTransactionCell");
		public UserTransaction Model { get; set; }

		public CustomTransactionCell (IntPtr handle) : base (handle)
		{
		}

		public static CustomTransactionCell Create ()
		{
			return (CustomTransactionCell)Nib.Instantiate (null, null) [0];
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			this.TransactionDescription.Text = Model.Description;
			this.TransactionDate.Text = Model.PosteoDate;
			this.TransactionAmount.Text = Model.Amount;
		}
	}
}

