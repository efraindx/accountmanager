﻿using System;
using BankAccountManager_MobileClient.Model;
using Newtonsoft.Json;
using System.IO;
using System.Linq;

namespace BankAccountManager_MobileClient
{
	public class JsonUtil
	{
		public UserAccount[] userAccounts { get; set; }

		//Method to get all accounts from json file
		public UserAccount[] GetUserAccounts() 
		{
			var json = File.ReadAllText ("Files/accounts.json");
			Console.WriteLine ("json: " + json);

			userAccounts = JsonConvert.DeserializeObject<UserAccount[]> (json);

			foreach (var account in userAccounts) {
				Console.WriteLine ("User Name: " + account.User);
				Console.WriteLine ("Firsft Transaction Amount: " + account.UserTransactions[0].Amount);
			}

			return userAccounts;
		}

		//Method to get transactions for a specific account
		public UserTransaction[] GetUserTransactionsByAccountId(int accountId)
		{
			userAccounts = GetUserAccounts ();
			return userAccounts.Where (a => a.UserAccountId == accountId).Single ().UserTransactions;
		}

	}
}

