﻿using System;

namespace BankAccountManager_MobileClient.Model
{
	public class UserAccount
	{
		public int UserAccountId { get; set; }
		public string User { get; set; }
		public string Alias { get; set; }
		public string Number { get; set; }
		public string Type { get; set; }
		public string Coin { get; set; }
		public long Balance { get; set; }
		public UserTransaction[] UserTransactions { get; set; }
	}
}

