﻿using System;

namespace BankAccountManager_MobileClient.Model
{
	public class UserTransaction
	{
		public int UserTransactionId { get; set; }
		public string PosteoDate { get; set; }
		public string EffectiveDate { get; set; }
		public string CheckNumber { get; set; }
		public string Description { get; set; }
		public string Amount { get; set; }
		public string Balance { get; set; }
		public int UserAccountId { get; set; }
		public UserAccount UserAccount { get; set; }
	}
}

