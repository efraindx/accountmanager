// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace BankAccountManager_MobileClient
{
	[Register ("Account")]
	partial class Account
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView AccountController { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (AccountController != null) {
				AccountController.Dispose ();
				AccountController = null;
			}
		}
	}
}
