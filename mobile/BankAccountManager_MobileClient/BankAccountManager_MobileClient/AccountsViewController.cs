using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using BankAccountManager_MobileClient.Model;

namespace BankAccountManager_MobileClient
{
	partial class AccountsViewController : UITableViewController
	{
		public JsonUtil jsonUtil { get; set; }
		public UserAccount[] userAccounts { get; set;}

		public AccountsViewController (IntPtr handle) : base (handle)
		{
			jsonUtil = new JsonUtil ();
			userAccounts = jsonUtil.GetUserAccounts ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			TableView.Source = new AccountsDataSource (this);
		}


		class AccountsDataSource : UITableViewSource
		{
			public AccountsViewController accountsController { get; set; }

			public AccountsDataSource (AccountsViewController accountsController)
			{
				this.accountsController = accountsController;
			}

			public override nint RowsInSection (UITableView tableview, nint section)
			{
				UserAccount[] userAccounts = accountsController.userAccounts;
				return userAccounts.Length;
			}

			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				UserAccount[] userAccounts = accountsController.userAccounts;
				var account = userAccounts [indexPath.Row];
				var cell = (CustomAccountCell)tableView.DequeueReusableCell (CustomAccountCell.Key);

				if (cell == null)
					cell = CustomAccountCell.Create ();

				cell.Model = account;

				return cell;
			}

			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				var selectedAccount = accountsController.userAccounts[indexPath.Row];

				TransactionsViewController transactionsController = accountsController.Storyboard.InstantiateViewController ("TransactionsViewController") as TransactionsViewController;
				transactionsController.SetSelectedAccount (selectedAccount);
				accountsController.NavigationController.PushViewController (transactionsController, true);
			}
		}
	}
}
