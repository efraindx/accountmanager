using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using BankAccountManager_MobileClient.Model;

namespace BankAccountManager_MobileClient
{
	partial class TransactionsViewController : UITableViewController
	{
		public JsonUtil jsonUtil { get; set; }
		public UserAccount selectedAccount { get; set; }
		public UserTransaction[] userTransactions { get; set; }

		public TransactionsViewController (IntPtr handle) : base (handle)
		{
			jsonUtil = new JsonUtil ();
		}

		public void SetSelectedAccount(UserAccount newAccount)
		{
			if (selectedAccount != newAccount) {
				this.selectedAccount = newAccount;
				ConfigureView ();
			}
		}

		public void ConfigureView()
		{
			if (IsViewLoaded && selectedAccount != null) {
				//Update view
				userTransactions = jsonUtil.GetUserTransactionsByAccountId (selectedAccount.UserAccountId);
			} 
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ConfigureView ();
			TableView.Source = new TransactionsControllerDataSource (this);
		}

		class TransactionsControllerDataSource : UITableViewSource
		{
			public TransactionsViewController transactionsController { get; set; }

			public TransactionsControllerDataSource(TransactionsViewController transactionsController)
			{
				this.transactionsController = transactionsController;
			}

			public override nint RowsInSection (UITableView tableview, nint section)
			{
				return transactionsController.userTransactions.Length;
			}

			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				UserTransaction[] userTransactions = transactionsController.userTransactions;
				var transaction = userTransactions [indexPath.Row];
				var cell = (CustomTransactionCell)tableView.DequeueReusableCell (CustomTransactionCell.Key);

				if (cell == null)
					cell = CustomTransactionCell.Create ();

				cell.Model = transaction;

				return cell;
			}
		}

	}
}
