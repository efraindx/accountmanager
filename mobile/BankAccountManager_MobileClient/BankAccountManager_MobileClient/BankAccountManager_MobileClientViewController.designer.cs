// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace BankAccountManager_MobileClient
{
	[Register ("BankAccountManager_MobileClientViewController")]
	partial class BankAccountManager_MobileClientViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField PasswordText { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton StartButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField UserText { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (PasswordText != null) {
				PasswordText.Dispose ();
				PasswordText = null;
			}
			if (StartButton != null) {
				StartButton.Dispose ();
				StartButton = null;
			}
			if (UserText != null) {
				UserText.Dispose ();
				UserText = null;
			}
		}
	}
}
