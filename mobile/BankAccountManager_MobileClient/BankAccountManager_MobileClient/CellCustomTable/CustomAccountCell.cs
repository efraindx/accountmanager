﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;

namespace BankAccountManager_MobileClient
{
	public class CustomAccountCell : UITableViewCell
	{
		UILabel aliasLabel, accountNumberLabel, typeLabel, coinLabel, amountLabel, 
			txtAlias, txtAccountNumber, txtType, txtCoin, txtAmount;

		public CustomAccountCell (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			SelectionStyle = UITableViewCellSelectionStyle.Blue;
			Accessory = UITableViewCellAccessory.DisclosureIndicator;
			ContentView.BackgroundColor = UIColor.Gray;

			aliasLabel = new UILabel () {
				Font = UIFont.FromName ("AmericanTypewriter", 15f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear,
				Text = "Alias:"
			};

			accountNumberLabel = new UILabel () { 
				Font = UIFont.FromName ("AmericanTypewriter", 15f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear,
				Text = "No. de Cuenta:"
			};

			typeLabel = new UILabel () { 
				Font = UIFont.FromName ("AmericanTypewriter", 15f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear,
				Text = "Tipo Cuenta:"
			};

			coinLabel = new UILabel () { 
				Font = UIFont.FromName ("AmericanTypewriter", 15f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear,
				Text = "Moneda:"
			};

			amountLabel = new UILabel () { 
				Font = UIFont.FromName ("AmericanTypewriter", 15f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear,
				Text = "Balance:"
			};

			txtAlias = new UILabel () { 
				Font = UIFont.FromName ("AmericanTypewriter", 15f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear
			};

			txtAccountNumber = new UILabel () { 
				Font = UIFont.FromName ("AmericanTypewriter", 15f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear
			};

			txtType = new UILabel () { 
				Font = UIFont.FromName ("AmericanTypewriter", 15f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear
			};

			txtCoin = new UILabel () { 
				Font = UIFont.FromName ("AmericanTypewriter", 15f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear
			};

			txtAmount = new UILabel () { 
				Font = UIFont.FromName ("AmericanTypewriter", 15f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear
			};

			ContentView.AddSubviews (new UIView[] {
				aliasLabel,
				accountNumberLabel,
				typeLabel,
				coinLabel,
				amountLabel,
				txtAlias,
				txtAccountNumber,
				txtType,
				txtCoin,
				txtAmount
			});
		}

		public void updateCell(string alias, string accountNumber, string type, string coin, string amount)
		{
			txtAlias.Text = alias;
			txtAccountNumber.Text = accountNumber;
			txtType.Text = type;
			txtCoin.Text = coin;
			txtAmount.Text = amount;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			aliasLabel.Frame = new CGRect ();
		}
	}
}

